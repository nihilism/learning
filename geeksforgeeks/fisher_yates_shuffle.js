function shuffle_func(arr){
  var set = Object.prototype.toString.call(arr) === '[object Array]' ? arr : [];
  var arrLength = set.length;
  var shuffled = Array(arrLength);
  var rand = function(start, end){
    if(end == null) {
      end = start;
      start = 0;
    }
    return start + Math.floor(Math.random()*(end - start + 1));
  };
  for(var i = 0, length = arrLength, rando; i < length; i++){
    rando = rand(0, i);
    if(rando !== i) {
      shuffled[i] = shuffled[rando];
    }
    shuffled[rando] = set[i];
  }
  return shuffled;
}

var arr = [1, 2, 3, 4, 5, 6, 7, 8, 9];

console.log(shuffle_func(arr));
console.log(shuffle_func(arr));
console.log(shuffle_func(arr));
