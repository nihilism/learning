var dynamicFib = function(n) {
    var fibList = [1, 1];
    if(n <= 1) {
        return 1 
    }
    for(var i = 2; i <=n; i++) {
        fibList[i] = fibList[fibList.length-2] + fibList[fibList.length-1];
    }
    return fibList[n];
}


console.log(dynamicFib(1));
console.log(dynamicFib(5));
console.log(dynamicFib(21));
console.log(dynamicFib(8));