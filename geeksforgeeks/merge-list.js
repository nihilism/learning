/**
 * Definition for an interval.
 * function Interval(start, end) {
 *     this.start = start;
 *     this.end = end;
 * }
 */
/**
 * @param {Interval[]} intervals
 * @return {Interval[]}
 */
var merge = function(intervals) {
  	var result = [];
  	intervals.sort(function(a, b){
  		return a.start - b.start;
  	});
  	var judge = function(interv1, interv2) {
  		if(interv2.start > interv1.end) {
  			return false;
  		} else {
  			return true;
  		}
  	}
  	var concat = function(interv1, interv2) {
  		var start = Math.min.call(null, interv1.start, interv2.start);
  		var end = Math.max.call(null, interv1.end, interv2.end);
 		interv1.start = start;
 		interv1.end = end;
 		return interv1;
  	}
  	for(var i = 0; i < intervals.length; i++) {
  		if(result.length === 0) {
  			result[result.length] = intervals[i];
  		} else if(judge(result[result.length-1], intervals[i])) {
  			result[result.length-1] = concat(result[result.length-1], intervals[i]);
  		} else {
  			result[result.length] = intervals[i];
  		}
  	}
  	return result;
};