    exports.multiDimensionalArray = function(dimension, n) {
        var initArr = new Array(n);
        function multi(dimen, arr, n) {
            if(dimen === 1) {
                return arr;
            } else {
                var tmp = new Array(n);
                for(var i = 0; i < n; i++) {
                    tmp[i] = clone(arr);
                }
                return multi(dimen-1, tmp, n);
            }
        }
        return multi(dimension, initArr, n);
    };
    exports.dualArray = function(m, n) {
        function multi(m) {
            var arr = new Array(m);
            for(var i = 0; i < m; i++) {
                var initArr = new Array(n);
                arr[i] = initArr;
            }
            return arr;
        }
        return multi(m);
    };
 exports.clone = function(obj) {
    var copy;

    // Handle the 3 simple types, and null or undefined
    if (null == obj || "object" != typeof obj) return obj;

    // Handle Date
    if (obj instanceof Date) {
        copy = new Date();
        copy.setTime(obj.getTime());
        return copy;
    }

    // Handle Array
    if (obj instanceof Array) {
        copy = [];
        for (var i = 0, len = obj.length; i < len; i++) {
            copy[i] = clone(obj[i]);
        }
        return copy;
    }

    // Handle Object
    if (obj instanceof Object) {
        copy = {};
        for (var attr in obj) {
            if (obj.hasOwnProperty(attr)) copy[attr] = clone(obj[attr]);
        }
        return copy;
    }

    throw new Error("Unable to copy obj! Its type isn't supported.");
}

exports.max = function(a, b) {
    return a >= b ? a : b;
}

exports.min = function(a, b) {
    return a > b ? b : a;
}