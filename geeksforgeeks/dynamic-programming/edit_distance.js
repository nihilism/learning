//https://en.wikipedia.org/wiki/Edit_distance

var util = require('./util.js');

var UNICOST = 1;

function ins() {
    return UNICOST;
}

function del() {
    return UNICOST;
}

function sub() {
    return UNICOST;
}

function editDistance(s1, s2) {
    if(typeof s1 !== 'string' || typeof s2 !== 'string') {
        throw new Error("you must enter two strings!");
    }
    if(s1.length === 0 || s2.length === 0) {
        return Math.max.call(null, s1.length, s2.length);
    }
    var distanceArray = util.dualArray(s1.length+1, s2.length+1);
    distanceArray[0][0] = 0;
    for(var i = 1; i < s1.length+1; i++) {
        distanceArray[i][0] = distanceArray[i-1][0] + del();
    }
    for(var j = 1; j < s2.length+1; j++) {
        distanceArray[0][j] = distanceArray[0][j-1] + ins();
    }
    for(i = 1; i < s1.length+1; i++) {
        for(j = 1; j < s2.length+1; j++) {
            if(s1[i-1] === s2[j-1]) {
                distanceArray[i][j] = distanceArray[i-1][j-1];
            } else {
                var substitution = distanceArray[i-1][j-1] + sub();
                var deletion = distanceArray[i-1][j] + del();
                var insertion = distanceArray[i][j-1] + ins();
                distanceArray[i][j] = Math.min.call(null, substitution, deletion, insertion);
            }
        }
    }
    return distanceArray[s1.length][s2.length];
}

exports.editDistance = editDistance;
