var util = require('./util.js');

var lcs = function(s1, s2) {
    if(s1.length === 0 || s2.length === 0) {
        return "";
    }
    var result = [];
    var dArray = util.dualArray(s1.length, s2.length);
    var maxLength = 0, maxIndex = 0;
    for(var i = 0; i < s1.length; i++) {
        for(var j = 0; j < s2.length; j++) {
            if(i === 0 || j === 0) {
                if(s1[i] === s2[j]) {
                    dArray[i][j] = 1;
                } else {
                    dArray[i][j] = 0;
                }
            };
            if(i && j) {
                if(s1[i] === s2[j]) {
                    dArray[i][j] = dArray[i-1][j-1] + 1;
                } else {
                    dArray[i][j] = 0;
                }
            }
            if(dArray[i][j] > maxLength) {
                maxLength = dArray[i][j];
                maxIndex = i + 1 - maxLength;
            }
        }
    }
    return s1.substr(maxIndex, maxLength);

}

console.log(lcs("yonglusyl", "sunyonglu"));

