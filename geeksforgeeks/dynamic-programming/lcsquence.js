var util = require('./util.js');

var lcs = function(s1, s2) {
    var dualArray = util.dualArray(s1.length, s2.length);
    for(var i = 0; i < s1.length; i++) {
        for(var j = 0; j < s2.length; j++) {
            if(i === 0 || j === 0) {
                if(s1[i] === s2[j]) {
                    dualArray[i][j] = 1;
                } else {
                    dualArray[i][j] = 0;
                }
            }
            if(i && j) {
                dualArray[i][j] = util.max(dualArray[i-1][j-1], util.max(dualArray[i-1][j], dualArray[i][j-1]));
                if(s1[i] === s2[j]) {
                    dualArray[i][j] += 1;
                }
            }
        }
    }
    var printLCS = function(arr, s1){
        var lcs = [];
        var w = arr.length-1, h = arr[0].length-1;
        while(w || h) {
            console.log(w, h);
            if(w ===0 || h === 0) {
                if(arr[w][h] === 1) {
                    lcs.push(s1[w]);
                }
                break;
            }
            if(arr[w][h] === arr[w-1][h-1]) {
                w = w - 1;
                h = h - 1;
            } else {
                if(arr[w][h] > util.max(arr[w][h-1], arr[w-1][h])) {
                    lcs.push(s1[w]);
                }
                if(arr[w][h-1] > arr[w-1][h]) {
                    w = w - 1;
                } else {
                    h = h - 1;
                }
            }
        }
        return lcs.reverse().join('');
    }
    console.log(printLCS(dualArray, s1));
    console.log(dualArray);
    return dualArray[s1.length-1][s2.length-1];
}

var lcsRecursive = function(s1, s2) {
    var recur = function(m, n, s1, s2) {
        if(m === 0 || n === 0) {
            return 0;
        }
        if(s1[m-1] === s2[n-1]) {
            return 1 + recur(m-1, n-1, s1, s2);
        } else {
            return util.max(recur(m-1, n, s1, s2), recur(m, n-1, s1, s2));
        }
    }
    return recur(s1.length, s2.length, s1, s2);
}


console.log(lcs('AGGTAB', 'GXTXAYB'));
console.log(lcsRecursive('AGGTAB', 'GXTXAYB'));