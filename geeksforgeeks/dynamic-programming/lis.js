 var lis = function(arr) {
    var tmp = Array.apply(null, Array(arr.length)).map(Number.prototype.valueOf, 1);
    var max = function(array, cmp) {
        var maximium = 1;
        for(var i = 0; i < array.length; i++) {
            if(cmp >= array[i] && (tmp[i] + 1) >= maximium) {
                tmp[array.length] = tmp[i] + 1;
                maximium = tmp[i] + 1;
            }
        }
    }
    for(var j = 0; j < arr.length-1; j++) {
        max(arr.slice(0, j+1), arr[j+1])
    }
    var lisNum = 0;
    for(var k = 0; k < tmp.length; k++) {
        if(tmp[k] > lisNum) {
            lisNum = tmp[k];
        }
    }
    return lisNum;
 }

 console.log(lis([10, 22, 9, 33, 21, 50, 41, 60, 80]))