function longestNonRepeatSubstring(s) {
    if(s.length == 1){
        return {longest: s, lpos: 1, ends: s, epos: 1};
    }
    var result = longestNonRepeatSubstring(s.substring(0, s.length-1));
    var longest = result.longest;
    var lpos = result.lpos;
    var ends = result.ends;
    var epos = result.epos;
    if(ends.indexOf(s[s.length-1]) < 0) {
        ends = ends + s[s.length-1];
        epos = epos + 1;
        if(longest.length <= ends.length) {
            longest = ends;
            lpos = epos;
        }
    } else {
        var rPos = ends.indexOf(s[s.length-1]);
        ends = ends.substring(rPos+1, ends.length) + s[s.length-1];
        epos = epos + 1;
    }
    return {longest: longest, lpos: lpos, ends: ends, epos: epos};
}

var m = 'biailing';
var r = longestNonRepeatSubstring(m);
console.log(r);
