var assert = require('assert');

var editDistance = require('../edit_distance');

describe('edit distance', function() {
    var tests = [
        {args: ['', ''],       expected: 0},
        {args: ['abc', ''],    expected: 3},
        {args: ['', 'ab'], expected: 2},
        {args: ['a', 'a'], expected: 0},
        {args: ['a', 'b'], expected: 1},
        {args: ['FOOD', 'MONEY'], expected: 4},
        {args: ['sunday', 'saturday'], expected: 3},
        {args: ['sunyong', 'syg'], expected: 4}
    ];

    tests.forEach(function(test) {
        it('correctly test "' + test.args[0] + '":"' + test.args[1]  + '"', function() {
            var res = editDistance.editDistance.apply(null, test.args);
            assert.equal(res, test.expected);

        });

    });

});
