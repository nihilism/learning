/**
 * @param {number[]} nums1
 * @param {number[]} nums2
 * @return {number}
 */
var findMedianSortedArrays = function(nums1, nums2) {
    var median = null,
    	middle1 = parseInt(nums1.length/2), start1 = 0, end1 = nums1.length-1,
    	middle2 = parseInt(nums2.length/2), start2 = 0, end2 = nums2.length-1;

    while(start1 !== end1 || start2 !== end2) {
 	   if(nums1[middle1] == nums2[middle2]) {
    		return nums1[middle1];
    	} else if(nums1[middle1] > nums2[middle2]) {
    		end1 = middle1;
    		start2 = middle2;
    	} else if(nums1[middle1] < nums2[middle2]) {
    		start1 = middle1;
    		end2 = middle2;
    	}
        middle1 = parseInt((start1+end1)/2);
        middle2 = parseInt((start2+end2)/2);
    	//console.log(nums1[middle1], nums1[start1], nums1[end1], nums2[middle2], nums2[start2], nums2[end2]);
    	console.log(middle1, start1, end1, middle2, start2, end2);
	}
	return nums1[middle1];
};


nums1 = [1,2,3,4,9,78,90];
nums2 = [1,2,9,10,32,75];
console.log(findMedianSortedArrays(nums1, nums2));