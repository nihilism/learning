var nthUglyNumber = function(n){
	var i2 = i3 = i5 = 0;
	var uglyList = [1];
	while(uglyList.length < n){
		var m2 = uglyList[i2]*2, 
			m3 = uglyList[i3]*3, 
			m5 = uglyList[i5]*5;
		var m = Math.min.apply(null, [m2, m3, m5]);
		if(m === m2) {
			i2 += 1;
		} 
		if (m === m3) {
			i3 += 1;
		}
		if (m === m5) {
			i5 += 1;
		}
		if(m > uglyList[uglyList.length-1]) {
			uglyList[uglyList.length] = m;
		}	
	}
	return uglyList[uglyList.length-1];
}


console.log(nthUglyNumber(10));